package com.picsart.executionservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.EnableKafka

@SpringBootApplication
@EnableKafka
class ApiApplication {
  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      runApplication<ApiApplication>(*args)
    }
  }
}