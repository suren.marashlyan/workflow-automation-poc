package com.picsart.executionservice.common.request

import javax.validation.constraints.Size

data class MetaData(
  @get:Size(min = 1, max = 200)
  val key: String,
  @get:Size(min = 1, max = 200)
  val value: String
)
