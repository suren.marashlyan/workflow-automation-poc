package com.picsart.executionservice.common

import org.springframework.stereotype.Component
import java.util.*

@Component
class UUIDGenerator() : UUIDGeneratorInterface {
    override fun new(): UUID {
        return UUID.randomUUID()
    }
}
