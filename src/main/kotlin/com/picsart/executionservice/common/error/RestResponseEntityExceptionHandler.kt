package com.picsart.executionservice.common.error

import com.picsart.executionservice.workflow.application.exception.WorkflowNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(
        value = [
            (WorkflowNotFoundException::class)
        ]
    )
    protected fun notFoundException(
        ex: Throwable?,
        request: WebRequest?
    ): ResponseEntity<ErrorsDetails> {
        val errorDetails = ErrorsDetails(
            ex?.message!!
        )
        return ResponseEntity(errorDetails, HttpStatus.NOT_FOUND)
    }
}
