package com.picsart.executionservice.common.error

data class ErrorsDetails(
    val message: String
)
