package com.picsart.executionservice.common

import java.util.*

interface UUIDGeneratorInterface {
    fun new(): UUID
}
