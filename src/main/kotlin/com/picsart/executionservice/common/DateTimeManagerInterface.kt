package com.picsart.executionservice.common

import java.time.LocalDateTime

interface DateTimeManagerInterface {
    fun now(): LocalDateTime
}
