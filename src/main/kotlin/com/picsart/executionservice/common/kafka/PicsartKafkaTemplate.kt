package com.picsart.executionservice.common.kafka

import com.picsart.executionservice.common.UUIDGeneratorInterface
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.internals.RecordHeader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.SendResult
import org.springframework.util.concurrent.ListenableFuture

class PicsartKafkaTemplate<K, V>(
    private val uuidGeneratorInterface: UUIDGeneratorInterface,
    producerFactory: ProducerFactory<K, V>
) : KafkaTemplate<K, V>(producerFactory) {
    private val myLogger: Logger = LoggerFactory.getLogger(PicsartKafkaTemplate::class.java)

    override fun send(topic: String, key: K, data: V): ListenableFuture<SendResult<K, V>> {
        val record = ProducerRecord(topic, key, data)

        myLogger.info("Trying to get txId from MDC")

        val txId = MDC.get("_txId") ?: uuidGeneratorInterface.new().toString()

        myLogger.info("Setting record txId header to : $txId")

        record.headers().add(RecordHeader("_txId", txId.toByteArray()))

        return super.send(record)
    }
}