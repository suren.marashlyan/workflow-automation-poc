package com.picsart.executionservice.com.picsart.executionservice.common.jpa.convertor

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.picsart.executionservice.workflow.domain.ReplayAction
import com.picsart.executionservice.workflow.domain.WorkflowTemplate
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class ReplayActionConverter : AttributeConverter<ReplayAction, String> {
    override fun convertToDatabaseColumn(replayAction: ReplayAction): String {
        return ObjectMapper().findAndRegisterModules().writeValueAsString(replayAction)
    }

    override fun convertToEntityAttribute(replayActionString: String?): ReplayAction {
        return ObjectMapper().findAndRegisterModules().readValue(replayActionString, object : TypeReference<ReplayAction>() {})
    }
}
