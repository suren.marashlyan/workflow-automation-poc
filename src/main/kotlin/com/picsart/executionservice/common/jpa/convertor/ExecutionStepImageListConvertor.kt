package com.picsart.executionservice.common.jpa.convertor

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class ExecutionStepImageListConvertor : AttributeConverter<List<String>?, String> {
  override fun convertToDatabaseColumn(images: List<String>?): String {
    return ObjectMapper().findAndRegisterModules().writeValueAsString(images)
  }

  override fun convertToEntityAttribute(replayActionString: String?): List<String>? {
    return ObjectMapper().findAndRegisterModules().readValue(replayActionString, object : TypeReference<List<String>?>() {})
  }
}