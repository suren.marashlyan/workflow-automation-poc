package com.picsart.executionservice.common.jpa.convertor

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.picsart.executionservice.workflow.domain.WorkflowTemplate
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class WorkflowTemplateConverter : AttributeConverter<WorkflowTemplate, String> {
    override fun convertToDatabaseColumn(workflowTemplate: WorkflowTemplate): String {
        return ObjectMapper().findAndRegisterModules().writeValueAsString(workflowTemplate)
    }

    override fun convertToEntityAttribute(workflowTemplateString: String?): WorkflowTemplate {
        return ObjectMapper().findAndRegisterModules().readValue(workflowTemplateString, object : TypeReference<WorkflowTemplate>() {})
    }
}
