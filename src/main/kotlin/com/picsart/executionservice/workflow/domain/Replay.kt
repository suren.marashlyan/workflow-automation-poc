package com.picsart.executionservice.workflow.domain

data class Replay(
  val actions: List<ReplayAction>
)