package com.picsart.executionservice.workflow.domain

data class WorkflowTemplate(
  val version: WorkflowVersion,
  val description: String,
  val replay: Replay,
  val source: List<String>,
)