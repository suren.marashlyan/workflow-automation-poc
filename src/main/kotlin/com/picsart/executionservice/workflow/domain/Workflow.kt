package com.picsart.executionservice.workflow.domain

import java.util.UUID

data class Workflow(
  val id: UUID,
  val name: String,
  val template: WorkflowTemplate,
  val createdAt: java.time.LocalDateTime
)
