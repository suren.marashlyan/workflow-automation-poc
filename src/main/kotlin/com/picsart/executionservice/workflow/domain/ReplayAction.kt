package com.picsart.executionservice.workflow.domain

class ReplayAction(
  val type: ActionType,
  val data: List<Pair<String, String>>
)