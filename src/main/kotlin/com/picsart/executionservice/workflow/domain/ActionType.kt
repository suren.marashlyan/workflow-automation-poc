package com.picsart.executionservice.workflow.domain

enum class ActionType {
  REMOVE_BACKGROUND,
  UPSCALE_ULTRA,
}