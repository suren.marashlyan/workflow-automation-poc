package com.picsart.executionservice.workflow.adapter.out.persistence

import com.picsart.executionservice.workflow.application.exception.WorkflowNotFoundException
import com.picsart.executionservice.workflow.application.port.out.LoadWorkflowPort
import com.picsart.executionservice.workflow.domain.*
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
class WorkflowInMemoryRepository: LoadWorkflowPort {

  val workflows: List<Workflow> = listOf(
    Workflow(
      UUID.fromString("3f396e97-d946-4f2d-93ee-615367168d04"),
      "Nice demo workflow",
      WorkflowTemplate(
        WorkflowVersion.V03032022,
        "Nice demo workflow template",
        Replay(
          listOf(
            ReplayAction(
              ActionType.REMOVE_BACKGROUND,
              listOf()
            ),
            ReplayAction(
              ActionType.UPSCALE_ULTRA,
              listOf(
                Pair("upscale_factor", "2")
              )
            )
          ),
        ),
        listOf(
          "https://www.boredpanda.com/blog/wp-content/uploads/2021/03/Photographer-takes-stunning-photos-of-peoples-interactions-with-animals-that-seem-to-have-come-out-of-a-fairy-tale-6058645cbace7__880.jpg",
          "http://inapcache.boston.com/universal/site_graphics/blogs/bigpicture/animals_people/bp1.jpg",
        )
      ),
      LocalDateTime.now().minusDays(2)
    )
  )

  override fun getById(id: UUID): Workflow {
    val workflows = this.workflows.filter { it.id == id }

    if (workflows.isEmpty()) {
      throw WorkflowNotFoundException.by(id)
    }

    return workflows.last()
  }

}