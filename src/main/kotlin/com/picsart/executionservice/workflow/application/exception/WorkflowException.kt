package com.picsart.executionservice.workflow.application.exception

open class WorkflowException(message: String) : Exception(message)