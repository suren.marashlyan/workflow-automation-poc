package com.picsart.executionservice.workflow.application.port.out

import com.picsart.executionservice.workflow.application.exception.WorkflowNotFoundException
import com.picsart.executionservice.workflow.domain.Workflow
import java.util.UUID
import kotlin.jvm.Throws

interface LoadWorkflowPort {
  @Throws(WorkflowNotFoundException::class)
  fun getById(id: UUID): Workflow
}