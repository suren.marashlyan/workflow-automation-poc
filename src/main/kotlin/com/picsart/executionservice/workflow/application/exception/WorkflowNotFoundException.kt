package com.picsart.executionservice.workflow.application.exception

import java.util.*

class WorkflowNotFoundException(message: String): WorkflowException(message) {
  companion object {
    fun by(id: UUID): WorkflowNotFoundException {
      return WorkflowNotFoundException("Workflow not found by id: $id")
    }
  }
}