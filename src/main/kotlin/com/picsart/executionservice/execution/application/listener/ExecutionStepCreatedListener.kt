package com.picsart.executionservice.com.picsart.executionservice.execution.application.listener

import com.picsart.executionservice.execution.application.port.out.EventPublisherInterface
import com.picsart.executionservice.execution.domain.event.ExecutionStepCreatedEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ExecutionStepCreatedListener(
  private val eventPublisher: EventPublisherInterface,
) {
  private val logger: Logger = LoggerFactory.getLogger(ExecutionStepCreatedListener::class.java)

  @EventListener
  @Transactional
  fun handler(event: ExecutionStepCreatedEvent) {
    logger.info("handling Execution step creation event, execution step id: ${event.executionStep.id}")

    eventPublisher.publish(event)
  }
}