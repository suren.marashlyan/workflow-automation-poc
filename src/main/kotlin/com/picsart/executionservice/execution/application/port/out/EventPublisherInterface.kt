package com.picsart.executionservice.execution.application.port.out

import com.picsart.executionservice.consumer.domain.event.ExecutionStepSucceededEvent
import com.picsart.executionservice.execution.domain.event.ExecutionStepCreatedEvent

interface EventPublisherInterface {
    fun publish(event: ExecutionStepCreatedEvent)
    fun publish(event: ExecutionStepSucceededEvent)
}
