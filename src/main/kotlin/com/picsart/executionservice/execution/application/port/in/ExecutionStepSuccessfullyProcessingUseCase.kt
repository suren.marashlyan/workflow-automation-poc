package com.picsart.executionservice.execution.application.port.`in`

interface ExecutionStepSuccessfullyProcessingUseCase {
  fun process(command: ExecutionStepSuccessfullyProcessingCommand)
}