package com.picsart.executionservice.execution.application.port.out

import com.picsart.executionservice.execution.domain.ExecutionStep

interface UpdateExecutionStepStatePort {
    fun save(executionStep: ExecutionStep)
}
