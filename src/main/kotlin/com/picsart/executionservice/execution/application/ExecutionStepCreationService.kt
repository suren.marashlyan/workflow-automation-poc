package com.picsart.executionservice.com.picsart.executionservice.execution.application

import com.picsart.executionservice.com.picsart.executionservice.execution.domain.ExecutionStepFactory
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepCreateCommand
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepCreationUseCase
import com.picsart.executionservice.execution.application.port.out.UpdateExecutionStepStatePort
import com.picsart.executionservice.execution.domain.ExecutionStep
import com.picsart.executionservice.execution.domain.event.ExecutionStepCreatedEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class ExecutionStepCreationService(
  private val executionStepFactory: ExecutionStepFactory,
  private val updateExecutionStepStatePort: UpdateExecutionStepStatePort,
  private val applicationEventPublisher: ApplicationEventPublisher,
): ExecutionStepCreationUseCase {
  override fun create(command: ExecutionStepCreateCommand): ExecutionStep {
    val executionStep = executionStepFactory.from(command)
    updateExecutionStepStatePort.save(executionStep)

    applicationEventPublisher.publishEvent(ExecutionStepCreatedEvent(this, executionStep))

    return executionStep
  }
}