package com.picsart.executionservice.com.picsart.executionservice.execution.application.listener

import com.picsart.executionservice.execution.application.port.`in`.ExecutionProcessingUseCase
import com.picsart.executionservice.execution.domain.event.ExecutionStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ExecutionStartListener(
  private val executionProcessingUseCase: ExecutionProcessingUseCase
) {
  @EventListener
  @Transactional
  fun handler(event: ExecutionStartedEvent) {
    executionProcessingUseCase.process(event.execution)
  }
}