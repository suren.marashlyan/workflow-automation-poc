package com.picsart.executionservice.execution.application

import com.picsart.executionservice.execution.domain.event.ExecutionStartedEvent
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStartCommand
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStartUseCase
import com.picsart.executionservice.execution.application.port.out.UpdateExecutionStatePort
import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.execution.domain.ExecutionFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class ExecutionStartService(
  private val executionFactory: ExecutionFactory,
  private val updateExecutionStatePort: UpdateExecutionStatePort,
  private val applicationEventPublisher: ApplicationEventPublisher,
): ExecutionStartUseCase {
  override fun start(command: ExecutionStartCommand): Execution {
    val execution = executionFactory.from(command)
    updateExecutionStatePort.save(execution)

    applicationEventPublisher.publishEvent(ExecutionStartedEvent(this, execution))

    return execution
  }
}