package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.execution.domain.ExecutionStep

class ExecutionStepSuccessfullyProcessingCommand(
  val executionStep: ExecutionStep,
  val result: List<String>
)