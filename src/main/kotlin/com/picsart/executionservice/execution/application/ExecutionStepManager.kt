package com.picsart.executionservice.execution.application

import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.execution.domain.ExecutionStep
import com.picsart.executionservice.workflow.domain.ReplayAction

interface ExecutionStepManager {
  fun hasNextStep(execution: Execution, executionSteps: List<ExecutionStep>): Boolean
  fun getNextStep(execution: Execution, executionSteps: List<ExecutionStep>): ReplayAction
}