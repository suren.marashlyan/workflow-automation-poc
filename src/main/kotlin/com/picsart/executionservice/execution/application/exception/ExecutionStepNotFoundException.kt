package com.picsart.executionservice.execution.application.exception

import java.util.*

class ExecutionStepNotFoundException(message: String): ExecutionStepException(message) {
  companion object {
    fun by(id: UUID): ExecutionStepNotFoundException {
      return ExecutionStepNotFoundException("Execution step not found by id: $id")
    }
  }
}