package com.picsart.executionservice.execution.application

import com.picsart.executionservice.execution.domain.event.ExecutionNextStepDiscoveredEvent
import com.picsart.executionservice.execution.application.port.`in`.ExecutionProcessingUseCase
import com.picsart.executionservice.execution.application.port.out.LoadExecutionStepPort
import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.execution.domain.event.ExecutionFinishedEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class ExecutionProcessingService(
  private val loadExecutionStepPort: LoadExecutionStepPort,
  private val executionStepManager: ExecutionStepManager,
  private val applicationEventPublisher: ApplicationEventPublisher,
): ExecutionProcessingUseCase {
  override fun process(execution: Execution) {
    val executionSteps = loadExecutionStepPort.findByExecutionId(execution.id)

    if (!executionStepManager.hasNextStep(execution, executionSteps)) {
      applicationEventPublisher.publishEvent(ExecutionFinishedEvent(this, execution))
      return
    }

    val nextStep = executionStepManager.getNextStep(execution, executionSteps)
    val source = if (executionSteps.isEmpty()) execution.workflowTemplate.source else executionSteps.last().result
    applicationEventPublisher.publishEvent(ExecutionNextStepDiscoveredEvent(this, execution, nextStep, source?: emptyList()))
  }
}