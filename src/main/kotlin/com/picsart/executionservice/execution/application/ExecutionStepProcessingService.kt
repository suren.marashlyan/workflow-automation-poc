package com.picsart.executionservice.com.picsart.executionservice.execution.application

import com.picsart.executionservice.common.DateTimeManagerInterface
import com.picsart.executionservice.execution.application.port.`in`.ExecutionProcessingUseCase
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepSuccessfullyProcessingCommand
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepSuccessfullyProcessingUseCase
import com.picsart.executionservice.execution.domain.ExecutionStepState
import org.springframework.stereotype.Service

@Service
class ExecutionStepProcessingService(
  private val executionProcessingUseCase: ExecutionProcessingUseCase,
  private val dateTimeManager: DateTimeManagerInterface
): ExecutionStepSuccessfullyProcessingUseCase {
  override fun process(command: ExecutionStepSuccessfullyProcessingCommand) {
    val executionStep = command.executionStep
    executionStep.finishedAt = dateTimeManager.now()
    executionStep.state = ExecutionStepState.FINISHED
    executionStep.result = command.result

    executionProcessingUseCase.process(executionStep.execution)
  }
}