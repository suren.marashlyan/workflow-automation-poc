package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.execution.domain.ExecutionStep

interface ExecutionStepCreationUseCase {
  fun create(command: ExecutionStepCreateCommand): ExecutionStep
}