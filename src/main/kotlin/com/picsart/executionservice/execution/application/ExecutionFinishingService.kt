package com.picsart.executionservice.execution.application

import com.picsart.executionservice.execution.application.port.`in`.ExecutionFinishingUseCase
import com.picsart.executionservice.common.DateTimeManagerInterface
import com.picsart.executionservice.execution.domain.Execution
import org.springframework.stereotype.Service

@Service
class ExecutionFinishingService(
  private val dateTimeManager: DateTimeManagerInterface
): ExecutionFinishingUseCase {
  override fun finish(execution: Execution): Execution {
    execution.complete(dateTimeManager.now())

    return execution
  }
}