package com.picsart.executionservice.com.picsart.executionservice.execution.application.listener

import com.picsart.executionservice.execution.application.port.`in`.ExecutionFinishingUseCase
import com.picsart.executionservice.execution.domain.event.ExecutionFinishedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ExecutionFinishedListener(
  private val executionFinishingUseCase: ExecutionFinishingUseCase
) {
  @EventListener
  @Transactional
  fun handler(event: ExecutionFinishedEvent) {
    executionFinishingUseCase.finish(event.execution)
  }
}