package com.picsart.executionservice.execution.application.port.out

import com.picsart.executionservice.execution.domain.ExecutionStep
import java.util.UUID

interface LoadExecutionStepPort {
  fun getById(id: UUID): ExecutionStep
  fun findByExecutionId(id: UUID): List<ExecutionStep>
}