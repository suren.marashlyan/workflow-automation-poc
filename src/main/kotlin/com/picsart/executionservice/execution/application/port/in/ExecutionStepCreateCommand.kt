package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.workflow.domain.ReplayAction

data class ExecutionStepCreateCommand(
  val execution: Execution,
  val action: ReplayAction,
  val source: List<String>
)