package com.picsart.executionservice.execution.application

import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.execution.domain.ExecutionStep
import com.picsart.executionservice.workflow.domain.ReplayAction
import org.springframework.stereotype.Service

@Service
class ExecutionStepManagerSimpleImplementation: ExecutionStepManager {
  override fun hasNextStep(execution: Execution, executionSteps: List<ExecutionStep>): Boolean {
    return execution.workflowTemplate.replay.actions.size > executionSteps.size
  }

  override fun getNextStep(execution: Execution, executionSteps: List<ExecutionStep>): ReplayAction {
    return execution.workflowTemplate.replay.actions.get(executionSteps.size)
  }
}