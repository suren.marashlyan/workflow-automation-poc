package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.execution.domain.Execution

interface ExecutionStartUseCase {
  fun start(command: ExecutionStartCommand): Execution
}