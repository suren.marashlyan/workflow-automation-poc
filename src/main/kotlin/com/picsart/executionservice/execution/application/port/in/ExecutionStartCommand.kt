package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.common.request.MetaData
import com.picsart.executionservice.workflow.domain.Workflow

data class ExecutionStartCommand(
  val workflow: Workflow,
  val metadata: List<MetaData>
)