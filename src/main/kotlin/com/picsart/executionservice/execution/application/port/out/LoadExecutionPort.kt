package com.picsart.executionservice.com.picsart.executionservice.execution.application.port.out

import com.picsart.executionservice.execution.domain.Execution
import java.util.UUID

interface LoadExecutionPort {
  fun findByWorkflowId(id: UUID): List<Execution>
}