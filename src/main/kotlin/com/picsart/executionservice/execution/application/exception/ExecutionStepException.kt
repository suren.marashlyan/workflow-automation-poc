package com.picsart.executionservice.execution.application.exception

open class ExecutionStepException(message: String) : Exception(message)