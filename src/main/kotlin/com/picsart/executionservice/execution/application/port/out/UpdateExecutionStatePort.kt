package com.picsart.executionservice.execution.application.port.out

import com.picsart.executionservice.execution.domain.Execution

interface UpdateExecutionStatePort {
    fun save(execution: Execution)
}
