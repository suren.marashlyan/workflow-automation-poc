package com.picsart.executionservice.com.picsart.executionservice.execution.application.listener

import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepCreateCommand
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepCreationUseCase
import com.picsart.executionservice.execution.domain.event.ExecutionNextStepDiscoveredEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ExecutionStepDiscoveredListener(
  private val executionStepCreationUseCase: ExecutionStepCreationUseCase
) {
  @EventListener
  @Transactional
  fun handler(event: ExecutionNextStepDiscoveredEvent) {
    executionStepCreationUseCase.create(ExecutionStepCreateCommand(event.execution, event.replayAction, event.images))
  }
}