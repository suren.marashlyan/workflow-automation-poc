package com.picsart.executionservice.execution.application.port.`in`

import com.picsart.executionservice.execution.domain.Execution

interface ExecutionFinishingUseCase {
  fun finish(execution: Execution): Execution
}