package com.picsart.executionservice.execution.domain

import com.picsart.executionservice.execution.domain.ExecutionState
import com.picsart.executionservice.common.DateTimeManagerInterface
import com.picsart.executionservice.common.UUIDGeneratorInterface
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStartCommand
import org.springframework.stereotype.Component

@Component
class ExecutionFactory(
  private val uuidGenerator: UUIDGeneratorInterface,
  private val dateTimeManager: DateTimeManagerInterface
) {
  fun from(command: ExecutionStartCommand): Execution {
    return Execution(
      this.uuidGenerator.new(),
      ExecutionState.PROCESSING,
      command.workflow.id,
      command.workflow.template,
      dateTimeManager.now()
    )
  }
}