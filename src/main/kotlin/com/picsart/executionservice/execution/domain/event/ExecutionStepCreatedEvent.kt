package com.picsart.executionservice.execution.domain.event

import com.picsart.executionservice.execution.domain.ExecutionStep
import org.springframework.context.ApplicationEvent

class ExecutionStepCreatedEvent(
    source: Any?,
    val executionStep: ExecutionStep
) : ApplicationEvent(source!!)
