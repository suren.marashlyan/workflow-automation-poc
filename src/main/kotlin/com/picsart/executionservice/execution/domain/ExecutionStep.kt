package com.picsart.executionservice.execution.domain

import com.picsart.executionservice.com.picsart.executionservice.common.jpa.convertor.ReplayActionConverter
import com.picsart.executionservice.common.jpa.convertor.ExecutionStepImageListConvertor
import com.picsart.executionservice.workflow.domain.ReplayAction
import org.hibernate.annotations.Type
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.*

@Entity
class ExecutionStep(
  @Id
  @Type(type = "uuid-char")
  val id: UUID,

  @OneToOne
  @JoinColumn(name = "execution_id")
  val execution: Execution,

  @Convert(converter = ReplayActionConverter::class)
  val action: ReplayAction,

  @Convert(converter = ExecutionStepImageListConvertor::class)
  var source: List<String>
) {
  var state: ExecutionStepState = ExecutionStepState.STARTED

  @Convert(converter = ExecutionStepImageListConvertor::class)
  var result: List<String>? = null
  var startedAt: LocalDateTime? = null
  var finishedAt: LocalDateTime? = null

  companion object {
    fun start(
      id: UUID,
      execution: Execution,
      action: ReplayAction,
      source: List<String>,
      startedAt: LocalDateTime
    ): ExecutionStep {
        val executionStep = ExecutionStep(id, execution, action, source)
        executionStep.state = ExecutionStepState.STARTED
        executionStep.startedAt = startedAt

        return executionStep
    }
  }
}