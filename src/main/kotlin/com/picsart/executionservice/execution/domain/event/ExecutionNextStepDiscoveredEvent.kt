package com.picsart.executionservice.execution.domain.event

import com.picsart.executionservice.execution.domain.Execution
import com.picsart.executionservice.workflow.domain.ReplayAction
import org.springframework.context.ApplicationEvent

class ExecutionNextStepDiscoveredEvent(
    source: Any?,
    val execution: Execution,
    val replayAction: ReplayAction,
    val images: List<String>
) : ApplicationEvent(source!!)
