package com.picsart.executionservice.execution.domain

enum class ExecutionState {
  VALIDATION,
  PROCESSING,
  FAILED,
  COMPLETED
}