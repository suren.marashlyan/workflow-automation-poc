package com.picsart.executionservice.execution.domain.event

import com.picsart.executionservice.execution.domain.Execution
import org.springframework.context.ApplicationEvent

class ExecutionStartedEvent(
    source: Any?,
    val execution: Execution
) : ApplicationEvent(source!!)
