package com.picsart.executionservice.com.picsart.executionservice.execution.domain

import com.picsart.executionservice.common.DateTimeManagerInterface
import com.picsart.executionservice.common.UUIDGeneratorInterface
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepCreateCommand
import com.picsart.executionservice.execution.domain.ExecutionStep
import org.springframework.stereotype.Component

@Component
class ExecutionStepFactory(
  private val uuidGenerator: UUIDGeneratorInterface,
  private val dateTimeManager: DateTimeManagerInterface
) {
  fun from(command: ExecutionStepCreateCommand): ExecutionStep {
    return ExecutionStep.start(
      this.uuidGenerator.new(),
      command.execution,
      command.action,
      command.source,
      dateTimeManager.now()
    )
  }
}