package com.picsart.executionservice.execution.domain

enum class ExecutionStepState {
  STARTED,
  FINISHED,
  FAILED,
}