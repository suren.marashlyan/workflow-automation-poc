package com.picsart.executionservice.execution.domain

import com.picsart.executionservice.common.jpa.convertor.WorkflowTemplateConverter
import com.picsart.executionservice.workflow.domain.WorkflowTemplate
import org.hibernate.annotations.Type
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Execution(
  @Id
  @Type(type = "uuid-char")
  val id: UUID,
  var executionState: ExecutionState = ExecutionState.PROCESSING,
  @Type(type = "uuid-char")
  var workflowId: UUID,
  @Convert(converter = WorkflowTemplateConverter::class)
  var workflowTemplate: WorkflowTemplate,
  val createdAt: java.time.LocalDateTime,
  var completedAt: java.time.LocalDateTime? = null,
) {
  fun complete(completedAt: LocalDateTime) {
    this.completedAt = completedAt
    this.executionState = ExecutionState.COMPLETED
  }
}
