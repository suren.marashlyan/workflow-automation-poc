package com.picsart.executionservice.com.picsart.executionservice.execution.adapter.`in`.web.controller

import com.picsart.executionservice.com.picsart.executionservice.execution.adapter.`in`.web.response.ExecutionStepResponse
import com.picsart.executionservice.execution.application.port.out.LoadExecutionStepPort
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*
import kotlin.collections.List

@RestController
class ExecutionStepController(
  val loadExecutionStepPort: LoadExecutionStepPort,
) {
  @GetMapping("/executions/{executionId}")
  @ResponseStatus(HttpStatus.CREATED)
  fun getAll(@PathVariable executionId: UUID): List<ExecutionStepResponse> {
    return loadExecutionStepPort.findByExecutionId(executionId).map { ExecutionStepResponse.from(it) }
  }
}