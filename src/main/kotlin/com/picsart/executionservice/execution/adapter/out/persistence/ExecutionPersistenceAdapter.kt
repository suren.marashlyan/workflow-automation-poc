package com.picsart.executionservice.execution.adapter.out.persistence

import com.picsart.executionservice.execution.application.port.out.UpdateExecutionStatePort
import com.picsart.executionservice.execution.domain.Execution
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface ExecutionPersistenceAdapter : CrudRepository<Execution, UUID>, UpdateExecutionStatePort {
    override fun save(execution: Execution)
}
