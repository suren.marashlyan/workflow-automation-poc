package com.picsart.executionservice.execution.adapter.out.persistence

import com.picsart.executionservice.execution.application.port.out.UpdateExecutionStepStatePort
import com.picsart.executionservice.execution.domain.ExecutionStep
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface ExecutionStepPersistenceAdapter : CrudRepository<ExecutionStep, UUID>, UpdateExecutionStepStatePort {
    override fun save(executionStep: ExecutionStep)
}
