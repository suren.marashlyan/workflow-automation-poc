package com.picsart.executionservice.execution.adapter.`in`.web.controller

import com.picsart.executionservice.com.picsart.executionservice.execution.application.port.out.LoadExecutionPort
import com.picsart.executionservice.execution.adapter.`in`.web.request.ExecutionStartRequest
import com.picsart.executionservice.execution.adapter.`in`.web.response.ExecutionResponse
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStartUseCase
import com.picsart.executionservice.workflow.application.port.out.LoadWorkflowPort
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid
import kotlin.collections.List

@RestController
class ExecutionController(
  val loadWorkflowPort: LoadWorkflowPort,
  val loadExecutionPort: LoadExecutionPort,
  val executionStartUseCase: ExecutionStartUseCase
) {
  @PostMapping("/workflows/{workflowId}/executions")
  @ResponseStatus(HttpStatus.CREATED)
  fun create(@PathVariable workflowId: UUID, @Valid @RequestBody request: ExecutionStartRequest): ExecutionResponse {
    return ExecutionResponse.from(
      executionStartUseCase.start(
        ExecutionStartRequest.toCommand(request, loadWorkflowPort.getById(workflowId))
      )
    )
  }

  @GetMapping("/workflows/{workflowId}/executions")
  @ResponseStatus(HttpStatus.CREATED)
  fun getAll(@PathVariable workflowId: UUID): List<ExecutionResponse> {
    return loadExecutionPort.findByWorkflowId(loadWorkflowPort.getById(workflowId).id).map { ExecutionResponse.from(it) }
  }
}