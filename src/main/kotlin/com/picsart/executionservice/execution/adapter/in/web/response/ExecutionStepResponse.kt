package com.picsart.executionservice.com.picsart.executionservice.execution.adapter.`in`.web.response

import com.picsart.executionservice.execution.domain.ExecutionStep
import com.picsart.executionservice.execution.domain.ExecutionStepState
import com.picsart.executionservice.workflow.domain.ReplayAction
import java.time.LocalDateTime
import java.util.UUID

data class ExecutionStepResponse(
  val id: UUID,
  val state: ExecutionStepState,
  val action: ReplayAction,
  val source: List<String>,
  val result: List<String>?,
  var startedAt: LocalDateTime? = null,
  var finishedAt: LocalDateTime? = null
) {
  companion object {
    fun from(executionStep: ExecutionStep): ExecutionStepResponse {
      return ExecutionStepResponse(
        executionStep.id,
        executionStep.state,
        executionStep.action,
        executionStep.source,
        executionStep.result,
        executionStep.startedAt,
        executionStep.finishedAt,
      )
    }
  }
}