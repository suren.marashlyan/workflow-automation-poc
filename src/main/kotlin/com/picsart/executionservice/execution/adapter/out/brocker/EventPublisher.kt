package com.picsart.executionservice.execution.adapter.out.brocker

import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage
import com.picsart.executionservice.common.DateTimeManager
import com.picsart.executionservice.common.UUIDGeneratorInterface
import com.picsart.executionservice.consumer.domain.event.ExecutionStepSucceededEvent
import com.picsart.executionservice.execution.application.port.out.EventPublisherInterface
import com.picsart.executionservice.execution.domain.event.ExecutionStepCreatedEvent
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class EventPublisher(
    private val uuidGenerator: UUIDGeneratorInterface,
    private val dateTimeManager: DateTimeManager,
    private val kafkaTemplate: KafkaTemplate<String, ExecutionStepEventMessage>
) : EventPublisherInterface {
    override fun publish(event: ExecutionStepCreatedEvent) {
        val eventMessage = ExecutionStepEventMessage(
            uuidGenerator.new(),
            ExecutionStepEventMessage.ExecutionStepMessage.map(event.executionStep),
            dateTimeManager.now(),
        )

        kafkaTemplate.send("WorkflowAutomation.${event.executionStep.action.type.name.lowercase()}", event.executionStep.id.toString(), eventMessage)
    }

    override fun publish(event: ExecutionStepSucceededEvent) {
        val eventMessage = ExecutionStepEventMessage(
            uuidGenerator.new(),
            ExecutionStepEventMessage.ExecutionStepMessage(
                event.executionStep.id,
                event.executionStep.executionId,
                event.executionStep.action,
                emptyList(),
                event.executionStep.result
            ),
            dateTimeManager.now(),
        )

        kafkaTemplate.send("WorkflowAutomation.ExecutionStepSucceeded", event.executionStep.id.toString(), eventMessage)
    }
}