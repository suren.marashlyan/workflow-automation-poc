package com.picsart.executionservice.execution.adapter.`in`.web.request

import com.picsart.executionservice.common.request.MetaData
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStartCommand
import com.picsart.executionservice.workflow.domain.Workflow

data class ExecutionStartRequest(
  val metadata: List<MetaData>? = emptyList()
) {
  companion object {
    fun toCommand(request: ExecutionStartRequest, workflow: Workflow): ExecutionStartCommand {
      return ExecutionStartCommand(workflow, request.metadata?: emptyList())
    }
  }
}