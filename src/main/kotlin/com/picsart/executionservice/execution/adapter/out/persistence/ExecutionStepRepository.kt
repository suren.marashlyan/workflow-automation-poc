package com.picsart.executionservice.execution.adapter.out.persistence

import com.picsart.executionservice.execution.application.exception.ExecutionStepNotFoundException
import com.picsart.executionservice.execution.application.port.out.LoadExecutionStepPort
import com.picsart.executionservice.execution.domain.ExecutionStep
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class ExecutionStepRepository(
  private val executionStepJPARepository: ExecutionStepJPARepository
): LoadExecutionStepPort {
  override fun getById(id: UUID): ExecutionStep {
    val executionStep = this.executionStepJPARepository.findById(id)
    if (!executionStep.isPresent) {
      throw ExecutionStepNotFoundException.by(id)
    }

    return executionStep.get()
  }

  override fun findByExecutionId(id: UUID): List<ExecutionStep> {
    return executionStepJPARepository.findByExecutionId(id)
  }
}

interface ExecutionStepJPARepository : CrudRepository<ExecutionStep, UUID>, JpaSpecificationExecutor<ExecutionStep> {
  override fun findById(id: UUID): Optional<ExecutionStep>
  fun findByExecutionId(executionId: UUID): List<ExecutionStep>
}
