package com.picsart.executionservice.execution.adapter.`in`.web.response

import com.picsart.executionservice.common.request.MetaData
import com.picsart.executionservice.execution.domain.ExecutionState
import com.picsart.executionservice.execution.domain.Execution
import java.time.LocalDateTime
import java.util.UUID

data class ExecutionResponse(
  val id: UUID,
  val state: ExecutionState,
  val metadata: List<MetaData>,
  val createdAt: LocalDateTime,
  val completedAt: LocalDateTime?,
) {
  companion object {
    fun from(execution: Execution): ExecutionResponse {
      return ExecutionResponse(
        execution.id,
        execution.executionState,
        emptyList(),
        execution.createdAt,
        execution.completedAt,
      )
    }
  }
}