package com.picsart.executionservice.execution.adapter.out.brocker.message

import com.picsart.executionservice.execution.domain.ExecutionStep
import com.picsart.executionservice.workflow.domain.ReplayAction
import java.time.LocalDateTime
import java.util.*

data class ExecutionStepEventMessage(
    val eventId: UUID,
    val executionStep: ExecutionStepMessage,
    val publishDate: LocalDateTime,
) {
    data class ExecutionStepMessage(
        val id: UUID,
        val executionId: UUID,
        val action: ReplayAction,
        val source: List<String>? = null,
        val result: List<String>? = null
    ) {
        companion object {
            fun map(executionStep: ExecutionStep): ExecutionStepMessage {
                return ExecutionStepMessage(
                    executionStep.id,
                    executionStep.execution.id,
                    executionStep.action,
                    executionStep.source
                )
            }
        }
    }
}