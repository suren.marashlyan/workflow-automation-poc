package com.picsart.executionservice.com.picsart.executionservice.execution.adapter.`in`.kafka

import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepSuccessfullyProcessingCommand
import com.picsart.executionservice.execution.application.port.`in`.ExecutionStepSuccessfullyProcessingUseCase
import com.picsart.executionservice.execution.application.port.out.LoadExecutionStepPort
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import javax.transaction.Transactional


@Component
class ExecutionListener(
    private val loadExecutionStepPort: LoadExecutionStepPort,
    private val executionStepSuccessfullyProcessingUseCase: ExecutionStepSuccessfullyProcessingUseCase
) {
    private val logger: Logger = LoggerFactory.getLogger(ExecutionListener::class.java)

    @KafkaListener(
        topics = ["WorkflowAutomation.ExecutionStepSucceeded"],
        groupId = "execution-step-succeeded",
        containerFactory = "kafkaListenerContainerFactory",
        concurrency = "1",
    )
    @Transactional
    fun onAddObjectRequest(
        @Payload message: ExecutionStepEventMessage,
        acknowledgment: Acknowledgment
    ) {
        logger.info("Start to execution consume ${message.executionStep.action.type.name} event")

        executionStepSuccessfullyProcessingUseCase.process(
            ExecutionStepSuccessfullyProcessingCommand(
                loadExecutionStepPort.getById(message.executionStep.id),
                message.executionStep.result?: emptyList()
            )
        )
        acknowledgment.acknowledge()
    }
}