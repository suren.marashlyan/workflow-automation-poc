package com.picsart.executionservice.com.picsart.executionservice.execution.adapter.out.persistence

import com.picsart.executionservice.com.picsart.executionservice.execution.application.port.out.LoadExecutionPort
import com.picsart.executionservice.execution.domain.Execution
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class ExecutionRepository(
  private val executionJPARepository: ExecutionJPARepository
): LoadExecutionPort {
  override fun findByWorkflowId(id: UUID): List<Execution> {
    return executionJPARepository.findByWorkflowId(id)
  }
}

interface ExecutionJPARepository : CrudRepository<Execution, UUID>, JpaSpecificationExecutor<Execution> {
  fun findByWorkflowId(workflowId: UUID): List<Execution>
}
