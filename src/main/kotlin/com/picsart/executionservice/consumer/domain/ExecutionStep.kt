package com.picsart.executionservice.com.picsart.executionservice.consumer.domain

import com.picsart.executionservice.workflow.domain.ReplayAction
import java.util.UUID

data class ExecutionStep(
  val id: UUID,
  val executionId: UUID,
  val action: ReplayAction,
  var result: List<String>,
)