package com.picsart.executionservice.consumer.domain.event

import com.picsart.executionservice.com.picsart.executionservice.consumer.domain.ExecutionStep
import org.springframework.context.ApplicationEvent

class ExecutionStepSucceededEvent(
  source: Any?,
  val executionStep: ExecutionStep
) : ApplicationEvent(source!!)
