package com.picsart.executionservice.consumer.adapter.`in`.kafka

import com.picsart.executionservice.com.picsart.executionservice.consumer.application.port.`in`.UpscaleUltraUseCase
import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage
import com.picsart.executionservice.consumer.application.port.`in`.RemoveBackgroundUseCase
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component


@Component
class Listener(
    private val removeBackgroundUseCase: RemoveBackgroundUseCase,
    private val upscaleUltraUseCase: UpscaleUltraUseCase
) {
    private val logger: Logger = LoggerFactory.getLogger(Listener::class.java)

    @KafkaListener(
        topics = ["WorkflowAutomation.remove_background"],
        groupId = "remove_background_consumer",
        containerFactory = "kafkaListenerContainerFactory",
        concurrency = "1",
    )
    fun onRemoveBackgroundRequest(
        @Payload message: ExecutionStepEventMessage,
        acknowledgment: Acknowledgment
    ) {
        logger.info("Start to consume ${message.executionStep.action.type.name} event")

        removeBackgroundUseCase.process(message.executionStep)
        acknowledgment.acknowledge()
    }

    @KafkaListener(
        topics = ["WorkflowAutomation.upscale_ultra"],
        groupId = "upscale_ultra_consumer",
        containerFactory = "kafkaListenerContainerFactory",
        concurrency = "1",
    )
    fun onUpscaleUltraRequest(
        @Payload message: ExecutionStepEventMessage,
        acknowledgment: Acknowledgment
    ) {
        logger.info("Start to consume ${message.executionStep.action.type.name} event")

        upscaleUltraUseCase.process(message.executionStep)
        acknowledgment.acknowledge()
    }
}