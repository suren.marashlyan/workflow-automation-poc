package com.picsart.executionservice.com.picsart.executionservice.consumer.application

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.picsart.executionservice.com.picsart.executionservice.consumer.application.port.`in`.UpscaleUltraUseCase
import com.picsart.executionservice.com.picsart.executionservice.consumer.domain.ExecutionStep
import com.picsart.executionservice.consumer.domain.event.ExecutionStepSucceededEvent
import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service


@Service
class UpscaleUltraService(
  @Value("\${workflow_automation.atomic_services_url}")
  private val baseUrl: String,
  @Value("\${workflow_automation.atomic_services_api_key}")
  private val apiKey: String,

  private val applicationEventPublisher: ApplicationEventPublisher,
): UpscaleUltraUseCase {

  private val client: HttpClient = HttpClientBuilder.create().build();

  override fun process(message: ExecutionStepEventMessage.ExecutionStepMessage) {

    val imageUrls = message.source?.map { makeRequest(it, message.action.data) }

    applicationEventPublisher.publishEvent(ExecutionStepSucceededEvent(this,
      ExecutionStep(
        message.id,
        message.executionId,
        message.action,
        imageUrls?: emptyList()
      )
    ))
  }

  private fun makeRequest(image: String, data: List<Pair<String, String>>): String {
    val post = HttpPost("${baseUrl}/upscale/ultra")

    val builder: MultipartEntityBuilder = MultipartEntityBuilder.create()
    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
    builder.addPart("image_url", StringBody(image, ContentType.MULTIPART_FORM_DATA))
    data.forEach { builder.addPart(it.first, StringBody(it.second, ContentType.MULTIPART_FORM_DATA)) }

    val entity: HttpEntity = builder.build()

    post.entity = entity
    post.addHeader("apikey", apiKey)
    val response: HttpResponse = client.execute(post)
    val responseBody: String = EntityUtils.toString(response.entity)
    val removeBackgroundResponse: RemoveBackgroundResponse = ObjectMapper().findAndRegisterModules().readValue(responseBody, object : TypeReference<RemoveBackgroundResponse>() {})

    return removeBackgroundResponse.data.url
  }

  data class RemoveBackgroundResponse(
    val data: RemoveBackgroundResponseData,
    val status: String
  ) {

    data class RemoveBackgroundResponseData(
      val id: String,
      val url: String,
    )
  }
}
