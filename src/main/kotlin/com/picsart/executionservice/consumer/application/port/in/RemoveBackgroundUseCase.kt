package com.picsart.executionservice.consumer.application.port.`in`

import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage

interface RemoveBackgroundUseCase {
  fun process(message: ExecutionStepEventMessage.ExecutionStepMessage)
}