package com.picsart.executionservice.com.picsart.executionservice.consumer.application.port.`in`

import com.picsart.executionservice.execution.adapter.out.brocker.message.ExecutionStepEventMessage

interface UpscaleUltraUseCase {
  fun process(message: ExecutionStepEventMessage.ExecutionStepMessage)
}