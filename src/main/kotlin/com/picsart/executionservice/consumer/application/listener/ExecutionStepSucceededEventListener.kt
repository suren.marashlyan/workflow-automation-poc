package com.picsart.executionservice.consumer.application.listener

import com.picsart.executionservice.consumer.domain.event.ExecutionStepSucceededEvent
import com.picsart.executionservice.execution.application.port.out.EventPublisherInterface
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ExecutionStepSucceededEventListener(
  private val eventPublisher: EventPublisherInterface,
) {
  private val logger: Logger = LoggerFactory.getLogger(ExecutionStepSucceededEventListener::class.java)

  @EventListener
  @Transactional
  fun handler(event: ExecutionStepSucceededEvent) {
    logger.info("handling Execution step succeeded application event, execution step id: ${event.executionStep.id}")

    eventPublisher.publish(event)
  }
}