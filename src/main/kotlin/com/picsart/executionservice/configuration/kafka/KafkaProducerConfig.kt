package com.picsart.executionservice.configuration.kafka

import com.picsart.executionservice.common.UUIDGeneratorInterface
import com.picsart.executionservice.common.kafka.PicsartKafkaTemplate
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.serializer.JsonSerializer

@Configuration
class KafkaProducerConfig(
    private val uuidGeneratorInterface: UUIDGeneratorInterface
) {
    @Value("\${KAFKA_BOOTSTRAP_SERVERS}")
    lateinit var bootstrapServer: String

    @Bean
    fun <T> producerFactory(): ProducerFactory<String, T> {
        val configProps: MutableMap<String, Any> = HashMap()
        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapServer
        configProps[JsonSerializer.ADD_TYPE_INFO_HEADERS] = false
        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = JsonSerializer::class.java
        return DefaultKafkaProducerFactory(configProps)
    }

    @Bean
    fun <T> kafkaTemplate(): KafkaTemplate<String, T> {
        return PicsartKafkaTemplate(uuidGeneratorInterface, producerFactory())
    }
}