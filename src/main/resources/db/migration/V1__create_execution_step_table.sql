create table execution_step
(
  id           varchar(255) not null
  primary key,
  action       json null,
started_at   datetime(6)  null,
finished_at  datetime(6)  null,
state        int          null,
execution_id varchar(255) null,
source json null,
result json null,
constraint execution_step_execution_id_fk
foreign key (execution_id) references execution (id)
);