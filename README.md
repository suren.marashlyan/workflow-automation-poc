# Workflow Automation Poc

This project includes POC for Workflow Automation to prove chosen architecture 

## [Architecture Specification](https://docs.google.com/document/d/1THbkejbAB9GFfd8IO6llVVmHMNeYu8t4uqTL4Dk1cUg/edit?usp=sharing&target=_blank)

## POC DEMO
[Video link](https://drive.google.com/file/d/1-Bw-DJeyAJwAEl0MumFzPagvgubx1u7F/view?target=_blank)

## Prerequisites
* Java v16
* Kotlin v1.6

## Getting started

Just run docker compose to up DB and KAFKA. The service can be run from the IDEA as well (if DB and KAFKA running),  
just by adding ENV variables in the idea configuration.
```shell
DB_URL=jdbc:mysql://localhost:3306/execution_service;DB_USERNAME=root;DB_PASSWORD=example;KAFKA_BOOTSTRAP_SERVERS=localhost:29092;spring.kafka.bootstrap-servers=localhost:29092
```
